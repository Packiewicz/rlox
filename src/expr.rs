use crate::token::{Literal, Token};

pub enum Expr<'a> {
	Literal(Literal),
	Unary(Token<'a>, Box<Expr<'a>>),
	Binary(Box<Expr<'a>>, Token<'a>, Box<Expr<'a>>),
	Grouping(Box<Expr<'a>>)
}