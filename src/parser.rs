use crate::token::{Token, Literal, TokenType};
use crate::expr::Expr;
use crate::scanner::Scanner;

pub struct Parser<'a> {
	pub scanner: &'a Scanner<'a>,

	current: usize
}

impl<'a> Parser<'a> {

	pub fn new(s: &'a Scanner<'a>) -> Parser<'a> {
		Parser {
			scanner: s,
			current: 0
		}
	}

	fn expression(&mut self) -> Expr {
		return self.equality();
	}

	fn equality(&mut self) -> Expr {
		let expr = self.comparison();

		while self.match_tokens(&[TokenType::BANG_EQUAL, TokenType::EQUAL_EQUAL]) {
			let operator = self.previous();
			let right = self.comparison();
			expr = Expr::Binary(Box::new(expr), operator.clone(), Box::new(right));
		}

		return expr;
	}

	fn comparison(&mut self) -> Expr {
		let expr = self.addition();

		while self.match_tokens(&[TokenType::GREATER, TokenType::GREATER_EQUAL,
			TokenType::LESS, TokenType::LESS_EQUAL]) {
			let operator = self.previous();
			let right = self.addition();
			expr = Expr::Binary(Box::new(expr), operator.clone(), Box::new(right));
		}

		return expr;
	}

	fn addition(&mut self) -> Expr {
		let mut expr = self.multiplication();

		while self.match_tokens(&[TokenType::MINUS, TokenType::PLUS]) {
			let operator = self.previous();
			let right = self.multiplication();
			expr = Expr::Binary(Box::new(expr), operator.clone(), Box::new(right));
		}

		return expr;
	}

	fn multiplication(&mut self) -> Expr {
		let expr = self.unary();

		while self.match_tokens(&[TokenType::SLASH, TokenType::STAR]) {
			let operator = self.previous();
			let right = self.unary();
			expr = Expr::Binary(Box::new(expr), operator.clone(), Box::new(right));
		}

		return expr;
	}

	fn unary(&mut self) -> Expr {
		if self.match_tokens(&[TokenType::BANG, TokenType::MINUS]) {
			let operator = self.previous().clone();
			let right = self.unary();
			return Expr::Unary(operator, Box::new(right));
		}

		return self.primary();
	}

	fn primary(&mut self) -> Expr {

		if self.match_tokens(&[TokenType::FALSE]) {
			return Expr::Literal(Literal::Bool(false));
		} else if self.match_tokens(&[TokenType::TRUE]) {
			return Expr::Literal(Literal::Bool(true));
		} else if self.match_tokens(&[TokenType::NIL]) {
			return Expr::Literal(Literal::Nil);
		} else if self.match_tokens(&[TokenType::NUMBER, TokenType::STRING]) {
			return Expr::Literal(self.previous().literal.clone());
		} else if self.match_tokens(&[TokenType::LEFT_PAREN]) {
			// todo error expected closing paren
			return Expr::Grouping(Box::new(self.expression()));
		} else {
			return Expr::Literal(Literal::Nil);
		}
	}

	fn match_tokens(&mut self, types: &[TokenType]) -> bool {
		for t_type in types {
			if self.check(t_type) {
				self.advance();
				return true;
			}
		}
		return false;
	}

	fn advance(&mut self) -> &Token {
		if !self.is_at_end() {
			self.current += 1;
		} 
		return self.previous();
	}

	fn check(&self, t_type: &TokenType) -> bool {
		if self.is_at_end() {
			return false;
		} else if self.scanner.tokens[self.current].token_type == *t_type {
			return true;
		} else {
			return false;
		}
	}

	fn is_at_end(&self) -> bool {
		return self.current >= self.scanner.tokens.len();
	}

	fn peek(&self, offset: usize) -> &Token {
		return &self.scanner.tokens[self.current + offset];
	}

	fn previous(&self) -> &Token {
		if self.current > 1 {
			return &self.scanner.tokens[self.current - 1];
		} else {
			return &self.scanner.tokens[0];
		}
	}

}