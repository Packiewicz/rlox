use std::vec::Vec;

#[derive(Debug)]
pub struct Error {
	pub line: usize,
	pub column: usize,
	pub msg: String,
}

struct Lox {
	errors: Vec<Error>,
}

impl Lox {
	pub fn error(&mut self, l: usize, col: usize, msg: String) {
		self.errors.push(Error {
			line: l,
			column: col,
			msg: msg,
		});
	}
}