use crate::lox::Error;
use crate::token::{Literal, Token, TokenType, KEYWORDS};
use std::char;
use std::str::FromStr;
use std::vec::Vec;

#[derive(Default)]
pub struct Scanner<'a> {
    pub source: &'a str,
    pub lex_errors: Vec<Error>,

    pub tokens: Vec<Token<'a>>,

    start: usize,
    current: usize,
    line: usize,
}

impl<'a> Scanner<'a> {

	pub fn new(src: &'a str) -> Scanner {
		Scanner {
			source: src,
			tokens: Vec::new(),
			lex_errors: Vec::new(),
			start: 0,
			current: 0,
			line: 0
		}
	}

    pub fn scan_tokens(&mut self) -> &Vec<Token> {

		self.start = 0;
		self.current = 0;
		self.line = 0;

        while !Self::is_at_end(self) {
            self.start = self.current;
            Self::scan_token(self);
        }

        self.tokens.push(Token {
            token_type: TokenType::EOF,
            line: self.line,
            literal: Literal::Nil,
            lexeme: "",
        });

        return &self.tokens;
    }

    fn is_at_end(&self) -> bool {
        return self.current >= self.source.len();
    }

    fn scan_token(&mut self) {
        let c = Self::advance(self);
        let ch: char;
        match c {
            Some(x) => ch = x,
            None => return,
        }

        match ch {
            // 1 char opearators
            '(' => Self::add_token(self, TokenType::LEFT_PAREN),
            ')' => Self::add_token(self, TokenType::RIGHT_PAREN),
            '{' => Self::add_token(self, TokenType::LEFT_BRACE),
            '}' => Self::add_token(self, TokenType::RIGHT_BRACE),
            '[' => Self::add_token(self, TokenType::LEFT_INDEX),
            ']' => Self::add_token(self, TokenType::RIGHT_INDEX),
            ',' => Self::add_token(self, TokenType::COMMA),
            '.' => Self::add_token(self, TokenType::DOT),
            '-' => Self::add_token(self, TokenType::MINUS),
            '+' => Self::add_token(self, TokenType::PLUS),
            ';' => Self::add_token(self, TokenType::SEMICOLON),
            '*' => Self::add_token(self, TokenType::STAR),
            // 1-2 char operators
            '!' => {
                if Self::advance_if_eq(self, '=') == true {
                    Self::add_token(self, TokenType::BANG_EQUAL)
                } else {
                    Self::add_token(self, TokenType::BANG)
                }
            }
            '=' => {
                if Self::advance_if_eq(self, '=') == true {
                    Self::add_token(self, TokenType::EQUAL_EQUAL)
                } else {
                    Self::add_token(self, TokenType::EQUAL)
                }
            }
            '<' => {
                if Self::advance_if_eq(self, '=') == true {
                    Self::add_token(self, TokenType::LESS_EQUAL)
                } else {
                    Self::add_token(self, TokenType::LESS)
                }
            }
            '>' => {
                if Self::advance_if_eq(self, '=') == true {
                    Self::add_token(self, TokenType::GREATER_EQUAL)
                } else {
                    Self::add_token(self, TokenType::GREATER)
                }
            }
            '/' => {
                if Self::advance_if_eq(self, '/') == true {
                    while Self::peek(self, 0) != '\n' && !self.is_at_end() {
                        self.advance();
                    }
                } else {
                    Self::add_token(self, TokenType::SLASH);
                }
            }

            '"' => self.string_literal(),

            // ignore whitespace
            '\t' | ' ' | '\r' => {}
            // line break
            '\n' => self.line += 1,

            _ => {
                if ch.is_digit(10) {
                    self.number_literal();
				} else if ch.is_alphabetic() || ch == '_' {
					self.identifier();
                } else {
                    self.lex_errors.push(Error {
                        line: self.line,
                        column: self.current,
                        msg: "Unrecognized token".to_string(),
                    })
                }
            }
        }
    }

	fn identifier(&mut self) {
		while self.peek(0).is_alphanumeric() || self.peek(0) == '_' {
			self.advance();
		}

		let token_type = KEYWORDS.get(&self.source[self.start..self.current]);
		match token_type {
			Some(x) => self.add_token(*x),
			None => self.add_token(TokenType::IDENTIFIER)
		}
	}

    fn number_literal(&mut self) {
        while self.peek(0).is_digit(10) {
            self.advance();
        }

        if self.peek(0) == '.' && self.peek(1).is_digit(10) {
            self.advance();
            while self.peek(0).is_digit(10) {
                self.advance();
            }
        }

        let number = f64::from_str(&self.source[self.start..self.current]).unwrap();
        self.add_token_val(TokenType::NUMBER, Literal::FloatingPoint(number));
    }

    fn string_literal(&mut self) {
        while self.peek(0) != '"' && !self.is_at_end() {
            if self.peek(0) == '\n' {
                self.line += 1;
            }
			self.advance();
        }

        if self.is_at_end() {
            self.lex_errors.push(Error {
                line: self.line,
                column: self.current,
                msg: "Unterminated string literal".to_string(),
            });
        }

        // the closing "
        self.advance();
        self.add_token_val(
            TokenType::STRING,
            Literal::LString(self.source[self.start + 1..self.current + 1].to_string()),
        );
    }

    // if theres no more chars in line return empty character
    fn peek(&self, how_far: usize) -> char {
        match self.source.chars().nth(self.current + how_far) {
            Some(x) => x,
            None => char::from_u32(0).unwrap(),
        }
    }

    fn advance_if_eq(&mut self, c: char) -> bool {
        let result = if Self::is_at_end(self) {
            false
        } else {
            return match self.source.chars().nth(self.current) {
                Some(x) => {
                    if x == c {
                        true
                    } else {
                        false
                    }
                }
                None => false,
            };
        };

        if result == true {
            self.current += 1;
        }
        return result;
    }

    fn advance(&mut self) -> Option<char> {
        self.current += 1;
        return self.source.chars().nth(self.current - 1);
    }

    fn add_token(&mut self, token: TokenType) {
        let tok = Token {
            token_type: token,
            lexeme: &self.source[self.start..self.current],
            literal: Literal::Nil,
            line: self.line,
        };
        self.tokens.push(tok);
    }

    fn add_token_val(&mut self, token: TokenType, lit: Literal) {
        let tok = Token {
            token_type: token,
            lexeme: &self.source[self.start..self.current],
            literal: lit,
            line: self.line,
        };
        self.tokens.push(tok);
    }
}
