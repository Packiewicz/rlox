use std::fmt;
use std::collections::HashMap;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum TokenType {
	// Single-character tokens.                      
  LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE,
  COMMA, DOT, MINUS, PLUS, SEMICOLON, SLASH, STAR, 
  LEFT_INDEX, RIGHT_INDEX,

  // One or two character tokens.                  
  BANG, BANG_EQUAL,
  EQUAL, EQUAL_EQUAL,
  GREATER, GREATER_EQUAL,
  LESS, LESS_EQUAL,

  // Literals.                                     
  IDENTIFIER, STRING, NUMBER,

  // Keywords.                                     
  AND, CLASS, ELSE, FALSE, FUN, FOR, IF, NIL, OR,
  PRINT, RETURN, SUPER, THIS, TRUE, VAR, WHILE,

  EOF
}

lazy_static! {
	pub static ref KEYWORDS: HashMap<&'static str, TokenType> = {
		let mut m = HashMap::new();
		m.insert("and", TokenType::AND);                       
		m.insert("class", TokenType::CLASS);                     
		m.insert("else", TokenType::ELSE);                      
		m.insert("false", TokenType::FALSE);                     
		m.insert("for", TokenType::FOR);                       
		m.insert("fun", TokenType::FUN);                       
		m.insert("if", TokenType::IF);                        
		m.insert("nil", TokenType::NIL);                       
		m.insert("or", TokenType::OR);                        
		m.insert("print", TokenType::PRINT);                     
		m.insert("return", TokenType::RETURN);                    
		m.insert("super", TokenType::SUPER);                     
		m.insert("this", TokenType::THIS);                      
		m.insert("true", TokenType::TRUE);                      
		m.insert("var", TokenType::VAR);                       
		m.insert("while", TokenType::WHILE);      
		m
	};
}

impl fmt::Display for TokenType {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{:?}", self)
	}
}

#[derive(Clone)]
pub enum Literal {
	Integer(i64),
	FloatingPoint(f64),
	LString(String),
	Bool(bool),
	Nil
}

#[derive(Clone)]
pub struct Token<'a> {
	pub token_type: TokenType,
	pub lexeme: &'a str,
	pub literal: Literal,
	pub line: usize,
}

impl<'a> Token<'a> {
	pub fn to_string(&self) -> String {
		let mut token = String::new();

		token.push_str(&self.token_type.to_string());
		token.push_str(": \n\tlexeme: ");
		token.push_str(&self.lexeme);
		token.push_str("\n\tline: ");
		token.push_str(&self.line.to_string());

		token.push_str("\n\tliteral: ");
		match &self.literal {
			Literal::Integer(i) => token.push_str(&i.to_string()),
			Literal::FloatingPoint(f) =>token.push_str(&f.to_string()), 
			Literal::LString(s) =>token.push_str(&s.to_string()),
			Literal::Bool(b) => token.push_str(&b.to_string()),
			Literal::Nil => token.push_str("Nil")
		}

		return token;
	}
}