#![allow(non_camel_case_types)]

#[macro_use]
extern crate lazy_static;

mod scanner;
mod token;
mod lox;
mod parser;
mod expr;
mod errors;

use std::env;
use std::io;
use crate::scanner::Scanner;

use std::fs::File;
use std::io::{BufReader, Read};

static mut HAD_ERROR: bool = false;

fn main() {
	let args: Vec<String> = env::args().collect();

	if args.len() > 2 {
		println!("Usage: rlox [script]\n");
		std::process::exit(64);
	} else if args.len() == 2 {
		match run_file(&args[1]) {
			Err(e) => println!("{}", e),
			Ok(..) => {}
		}
	} else {
		run_prompt();
	}
}

fn run_file(script: &str) -> std::result::Result<(), std::io::Error> {
	let file = File::open(script)?;
	let mut buff = BufReader::new(file);

	let mut contents = String::new();

	buff.read_to_string(&mut contents)?;

	let mut scanner = Scanner::new(&contents);
	scanner.scan_tokens();

	for token in scanner.tokens {
		println!("{}", token.to_string());
	}

	for error in scanner.lex_errors {
		println!("{:?}", error);
	}

	Ok(())
}

fn run_prompt() {
	let mut line = String::new();

	loop {
		match io::stdin().read_line(&mut line) {
			Err(error) => println!("Input error {}", error),
			Ok(_n) => {
				let mut scanner = Scanner::new(&line);
				scanner.scan_tokens();

				for token in scanner.tokens {
					println!("{}", token.to_string());
				}

				for error in scanner.lex_errors {
					println!("{:?}", error);
				}
			}
		}

		if run(&line) == true {
			return;
		}

		unsafe {
			if HAD_ERROR == true {
				std::process::exit(65);
			}
		}
	}
}

fn run(line: &str) -> bool {
	if line == "exit" {
		return true;
	} else {
		return false;
	}
}
